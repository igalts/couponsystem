﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using coupon_system.Models;

namespace coupon_system.DAL
{

    public class CouponSystemInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<CouponSystemContext>
    {
        protected override void Seed(CouponSystemContext context)
        {
            var coupon = new List<Coupon>
            {
            new Coupon{ID="1",name="Alexander",description="FML",Category=Categories.Atractions,orriginalPrice=99.99,disclPrice=88.88,rate=10,lastDate=DateTime.Parse("2005-09-01"),totalOrders=10},
            new Coupon{ID="2",name="Igal",description="FML",Category=Categories.Hotels,orriginalPrice=99.99,disclPrice=88.88,rate=10,lastDate=DateTime.Parse("2005-09-01"),totalOrders=101},
            };

            coupon.ForEach(s => context.Coupons.Add(s));
            context.SaveChanges();

            var sysManger = new List<SystemManger>
            {
            new SystemManger{userName="Yoni",password="1234",mail="yoni@asd.com",phone = "097667488"},
            };
            sysManger.ForEach(s => context.SystemMangers.Add(s));
            context.SaveChanges();


            var seller = new List<Seller>
            {
            new Seller{ userName="Or",password="122234",mail="or@asd.com",phone = "0976611188"},
            new Seller{ userName="Omer",password="5643",mail="omer@asd.com",phone = "0972341188"},
            };
            seller.ForEach(s => context.Sellers.Add(s));
            context.SaveChanges();


            //i did NOOOT init the list of enums here not sure if i care
            var buyer = new List<Buyer>
            {
            new Buyer{userName="ben",password="122234",mail="ben@asd.com",phone = "0976612188",currentLocation="gaza strip",lastNotify=DateTime.Parse("2005-09-01") },
            new Buyer{userName="dov",password="122234",mail="dov@asd.com",phone = "0976612188",currentLocation="gaza strip",lastNotify=DateTime.Parse("2005-09-01") },
            };
            buyer.ForEach(s => context.Buyers.Add(s));
            context.SaveChanges();

            var order = new List<Order>
            {
            new Order{serialKey="67865",openOrder=true },
            new Order{serialKey="50987",openOrder=false },
            };
            order.ForEach(s => context.Orders.Add(s));
            context.SaveChanges();

            var business = new List<Business>
            {
            new Business{ID="4569",name="Dildos united", address="rager" , city="beer shveva", description="mucho interesante", Category=Categories.TV,totalOrders=99, rate= 5.5 },
            new Business{ID="456876",name="altizahen", address="cloud" , city="city of angels", description="no mucho interesante", Category=Categories.TV,totalOrders=1111, rate= 33.3 },
            };
            business.ForEach(s => context.Businesses.Add(s));
            context.SaveChanges();
        }
    }
}