﻿using coupon_system.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace coupon_system.DAL
{
    public class CouponSystemContext : DbContext
    {
        public CouponSystemContext()
            : base("CouponSystemContext")
        {
        }

        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<SystemManger> SystemMangers { get; set; }
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Buyer> Buyers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Business> Businesses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}