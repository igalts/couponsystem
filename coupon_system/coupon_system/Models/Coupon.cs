﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace coupon_system.Models
{
    public class Coupon
    {

        public string ID { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Categories? Category { get; set; }
        public double orriginalPrice { get; set; }
        public double disclPrice { get; set; }
        public double rate { get; set; }
        public DateTime lastDate { get; set; }
        public int totalOrders { get; set; }

        public virtual Business Business { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}