﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace coupon_system.Models
{
    public class Seller
    {

        [Key]
        public string userName { get; set; }
        public string password { get; set; }
        public string mail { get; set; }
        public string phone { get; set; }

        public virtual ICollection<Business> Businesses { get; set; }
    }
}