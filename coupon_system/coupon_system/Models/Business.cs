﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace coupon_system.Models
{
    public enum Categories
    {
        Food,Hotels,Sport,TV,Atractions
    }
    public class Business
    {
        public string ID { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string description { get; set; }
        public Categories? Category { get; set; }
        public int totalOrders { get; set; }
        public double rate { get; set; }

        public virtual ICollection<Coupon> Coupons { get; set; }
        public virtual Seller Seller { get; set; }
    }
}