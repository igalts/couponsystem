﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace coupon_system.Models
{
    public class Order
    {
        [Key]
        public string serialKey { get; set; }
        public bool openOrder { get; set; }

        public virtual Coupon Coupon { get; set; }
        public virtual Buyer Buyer { get; set; }
    }
}