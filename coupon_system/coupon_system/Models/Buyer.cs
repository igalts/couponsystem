﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace coupon_system.Models
{
    public class Buyer
    {

        [Key]
        public string userName { get; set; }
        public string password { get; set; }
        public string mail { get; set; }
        public string phone { get; set; }
        public ICollection<Categories> favoriteCategories { get; set; }
        public string  currentLocation { get; set; }
        public DateTime lastNotify { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}