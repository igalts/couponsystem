﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using coupon_system.DAL;
using coupon_system.Models;

namespace coupon_system.Controllers
{
    public class SystemMangerController : Controller
    {
        private CouponSystemContext db = new CouponSystemContext();

        // GET: SystemManger
        public ActionResult Index()
        {
            return View(db.SystemMangers.ToList());
        }

        // GET: SystemManger/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemManger systemManger = db.SystemMangers.Find(id);
            if (systemManger == null)
            {
                return HttpNotFound();
            }
            return View(systemManger);
        }

        // GET: SystemManger/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SystemManger/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "userName,password,mail,phone")] SystemManger systemManger)
        {
            if (ModelState.IsValid)
            {
                db.SystemMangers.Add(systemManger);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(systemManger);
        }

        // GET: SystemManger/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemManger systemManger = db.SystemMangers.Find(id);
            if (systemManger == null)
            {
                return HttpNotFound();
            }
            return View(systemManger);
        }

        // POST: SystemManger/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "userName,password,mail,phone")] SystemManger systemManger)
        {
            if (ModelState.IsValid)
            {
                db.Entry(systemManger).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(systemManger);
        }

        // GET: SystemManger/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemManger systemManger = db.SystemMangers.Find(id);
            if (systemManger == null)
            {
                return HttpNotFound();
            }
            return View(systemManger);
        }

        // POST: SystemManger/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            SystemManger systemManger = db.SystemMangers.Find(id);
            db.SystemMangers.Remove(systemManger);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
