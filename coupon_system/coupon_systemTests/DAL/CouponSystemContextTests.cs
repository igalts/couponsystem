﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using coupon_system.DAL;
using coupon_system.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace coupon_system.DAL.Tests
{
    [TestClass()]
    public class CouponSystemContextTests
    {
        [TestMethod()]
        public void CouponSystemContextTest()
        {
            CouponSystemContext context = new CouponSystemContext();
            var buyer = new List<Buyer>
            {
            new Buyer{userName= "ert",password="122234",mail="ben@asd.com",phone = "0976612188",currentLocation="gaza strip",lastNotify=DateTime.Parse("2005-09-01") },
            new Buyer{userName="gfd",password="122234",mail="dov@asd.com",phone = "0976612188",currentLocation="gaza strip",lastNotify=DateTime.Parse("2005-09-01") },
            };
            buyer.ForEach(s => context.Buyers.Add(s));
            context.SaveChanges();
            Buyer buy1 = context.Buyers.Find("ert");
            context.Buyers.Remove(buy1);
            context.SaveChanges();
            Buyer buy2 = context.Buyers.Find("gfd");
            Assert.AreEqual(buy2.userName, "gfd");
            buy1 = context.Buyers.Find("gfd");
            context.Buyers.Remove(buy1);
            context.SaveChanges();


        }
    }
}
